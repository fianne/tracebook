package org.eclipse.tracecompass.incubator.tracebook.ui.view;

import java.util.Objects;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.tracecompass.incubator.internal.tmf.ui.multiview.ui.view.IMultiViewer;
import org.eclipse.tracecompass.incubator.internal.tmf.ui.multiview.ui.view.xychart.ChartMultiViewer;
import org.eclipse.tracecompass.incubator.tracebook.ui.controls.Section;
import org.eclipse.tracecompass.incubator.tracebook.ui.providers.ProviderPresenter;
import org.eclipse.tracecompass.internal.tmf.ui.viewers.xychart.TmfXYChartTimeAdapter;
import org.eclipse.tracecompass.tmf.core.dataprovider.IDataProviderDescriptor;
import org.eclipse.tracecompass.tmf.core.dataprovider.IDataProviderDescriptor.ProviderType;
import org.eclipse.tracecompass.tmf.core.trace.ITmfTrace;
import org.eclipse.tracecompass.tmf.ui.signal.TmfTimeViewAlignmentInfo;
import org.eclipse.tracecompass.tmf.ui.viewers.TmfViewer;
import org.eclipse.tracecompass.tmf.ui.widgets.timegraph.widgets.ITimeDataProvider;

@SuppressWarnings("restriction")
final class SectionForMultiViewer extends Section implements IMultiViewer{
	private IDataProviderDescriptor fProvider;
	private TmfViewer fViewer;

	public SectionForMultiViewer(Composite parent, IDataProviderDescriptor provider, ITmfTrace trace) {
		super(parent, parent.getBackground(), ProviderPresenter.getInstance().presentationFor(provider));
		this.fProvider = provider;

		if (provider.getType() == ProviderType.TREE_TIME_XY) {
			createXYGraph(trace);
		} else if(provider.getType() == ProviderType.TIME_GRAPH) {
			createTimeGraph(trace);
		} else {
			Label label = new Label(this, SWT.NONE);
			label.setText("Not supported");
			GridDataFactory.fillDefaults().grab(true, true).applyTo(label);
		}
	}
	@Override
	public void performAlign(int offset, int width) {
		// TODO Auto-generated method stub
	}

	@Override
	public TmfTimeViewAlignmentInfo getTimeViewAlignmentInfo() {
		// TODO Auto-generated method stub
        return new TmfTimeViewAlignmentInfo(getShell(), toDisplay(0, 0), 0);
	}

	@Override
	public int getAvailableWidth(int requestedOffset) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		return fProvider.getName();
	}

	@Override
	public void dispose() {
		if (isDisposed()) return;
		Composite parent = getParent();
		super.dispose();
		if (parent != null  && !parent.isDisposed()) {
			parent.layout(true);
		}
	}

	private void createTimeGraph(ITmfTrace trace) {
		// TODO Auto-generated method stub
		Label label = new Label(this, SWT.CENTER);
		label.setText("Timegraph");
		GridDataFactory.fillDefaults().grab(true, true).applyTo(label);
	}

	private void createXYGraph(ITmfTrace trace) {
        Composite composite = new Composite(this, SWT.NONE);
        GridDataFactory.fillDefaults().grab(true, true).applyTo(composite);
        composite.setLayout(new FillLayout());
        composite.setBackground(getBackground());
        ChartMultiViewer viewer = new ChartMultiViewer(composite, fProvider.getId());
        //viewer.setStatusLineManager(getViewSite().getActionBars().getStatusLineManager());
        if (trace != null) {
            viewer.loadTrace(trace);
        }
        // A workaround for XYCharts to realign after a selection
        // changes leading to possible changing of Y axis labels' width.
//        if (viewer.getLeftChildViewer() instanceof AbstractSelectTreeViewer2) {
//            AbstractSelectTreeViewer2 tree = (AbstractSelectTreeViewer2) viewer.getLeftChildViewer();
//            tree.addSelectionChangeListener(e -> alignViewers(false));
//        }
        fViewer = viewer;
	}

	public ITimeDataProvider getTimeDataProvider() {
		if (fViewer instanceof ChartMultiViewer) {
			ChartMultiViewer viewer = (ChartMultiViewer) fViewer;
			TmfXYChartTimeAdapter timeProvider = new TmfXYChartTimeAdapter(Objects.requireNonNull(viewer.getChartViewer()));
			return timeProvider;
		}
		return null;
	}
}