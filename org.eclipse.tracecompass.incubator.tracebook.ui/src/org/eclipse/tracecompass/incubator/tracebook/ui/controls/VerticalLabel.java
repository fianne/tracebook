package org.eclipse.tracecompass.incubator.tracebook.ui.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

public class VerticalLabel extends Canvas {
    private String text = "";
    public VerticalLabel(Composite parent) {
        super(parent, SWT.NONE);
        addPaintListener(e -> paintLabel(e));
    }

    public void setText(String text) {
        this.text = text;
    }

    private void paintLabel(PaintEvent e) {
        Point textExtent = e.gc.textExtent(text);
        Transform t = new Transform(getDisplay());
        t.translate((e.width - textExtent.y) / 2, (e.height + textExtent.x) / 2);
        t.rotate(-90);
        float[] p = new float[] {0, 0};
        t.transform(p);
        e.gc.setAdvanced(true);
        e.gc.setTransform(t);
        e.gc.drawText(text, e.x, e.y);
    }
}