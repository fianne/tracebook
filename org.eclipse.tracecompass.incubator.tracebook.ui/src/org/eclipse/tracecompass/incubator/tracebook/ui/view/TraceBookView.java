package org.eclipse.tracecompass.incubator.tracebook.ui.view;

import java.util.List;
import java.util.Objects;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.tracecompass.incubator.internal.tmf.ui.multiview.ui.view.AbstractMultiView;
import org.eclipse.tracecompass.incubator.internal.tmf.ui.multiview.ui.view.IMultiViewer;
import org.eclipse.tracecompass.internal.tmf.ui.viewers.xychart.TmfXYChartTimeAdapter;
import org.eclipse.tracecompass.tmf.core.dataprovider.DataProviderManager;
import org.eclipse.tracecompass.tmf.core.dataprovider.IDataProviderDescriptor;
import org.eclipse.tracecompass.tmf.core.trace.ITmfTrace;
import org.eclipse.tracecompass.tmf.ui.widgets.timegraph.widgets.Utils.TimeFormat;
import org.eclipse.ui.dialogs.ListSelectionDialog;

/**
 * A View that can display a Tracebook, i.e. a collection of charts from different providers.
 *
 * @author Fabrizio Iannetti
 *
 */
@SuppressWarnings("restriction") // TODO: move/modify MultiView logic here or extend MultiView and make API
public class TraceBookView extends AbstractMultiView {

	public static final String ID = "org.eclipse.tracecompass.incubator.tmf.ui.tracebook.TreaceBookView";

	public TraceBookView() {
		super(ID);
	}

	@Override
	protected void partControlCreated(Composite mainComposite, SashForm sashForm) {
		hideTimeScales();
	}

	@Override
	protected void createMenuItems() {
        IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
        menuManager.add(createAddSectionAction());
	}

	private IAction createAddSectionAction() {
		return new Action("Add Section", IAction.AS_PUSH_BUTTON) {
            @Override
            public void run() {
            	openDataProviderDialog(getTrace());
            }
		};
	}

	private void openDataProviderDialog(ITmfTrace trace) {
    	Shell shell = getViewSite().getWorkbenchWindow().getShell();
    	List<IDataProviderDescriptor> providers = getProvidersList(trace);
		ListSelectionDialog dialog = ListSelectionDialog.of(providers)
				.labelProvider(new LabelProvider() {
					@Override
					public String getText(Object element) {
						IDataProviderDescriptor provider = (IDataProviderDescriptor) element;
						return provider.getName();
					}
				})
				.title("Add Section")
				.message("Select the data provider for the new section")
				.create(shell);
    	if (dialog.open() == Window.OK) {
    		Object[] selected = dialog.getResult();
    		for (Object object : selected) {
				IDataProviderDescriptor provider = (IDataProviderDescriptor) object;
				SectionForMultiViewer lane = new SectionForMultiViewer(getLanesParent(), provider, getTrace());
				addSection(lane);
				lane.onClose(e -> removeLane(lane));
			}
    	}
    }

	private void addSection(SectionForMultiViewer section) {
		if (!hasLanes()) {
			setTimeProvider(section.getTimeDataProvider());
			section.onPaint(() -> redrawTimeScales());
		}
		addLane(section);
//        if (!hasLanes()) {
//        	viewer.getChartViewer().getSwtChart().addPaintListener(e -> redrawTimeScales());
//            TmfXYChartTimeAdapter timeProvider = new TmfXYChartTimeAdapter(Objects.requireNonNull(viewer.getChartViewer()));
//            timeProvider.setTimeFormat(TimeFormat.CALENDAR.convert());
//            setTimeProvider(timeProvider);
//        }
	}

	private List<IDataProviderDescriptor> getProvidersList(ITmfTrace trace) {
    	List<IDataProviderDescriptor> descriptors =
    		DataProviderManager.getInstance().getAvailableProviders(trace);
    	return descriptors;
    }
}
