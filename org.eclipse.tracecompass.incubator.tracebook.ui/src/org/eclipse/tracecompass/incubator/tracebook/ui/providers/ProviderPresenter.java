package org.eclipse.tracecompass.incubator.tracebook.ui.providers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Image;
import org.eclipse.tracecompass.incubator.tracebook.ui.Activator;
import org.eclipse.tracecompass.internal.analysis.os.linux.core.cpuusage.CpuUsageDataProvider;
import org.eclipse.tracecompass.tmf.core.dataprovider.IDataProviderDescriptor;

/**
 * This class returns presentation information (name, icon) for known data providers.
 *
 * @author fab
 *
 */
@SuppressWarnings("restriction")
public class ProviderPresenter {
	private static ProviderPresenter fInstance = new ProviderPresenter();

	// map providerId -> presentation info
	private Map<String, ProviderPresentationInfo> fInfo = new HashMap<>();

	private ProviderPresenter() {
		// cpu provider
		fInfo.put(CpuUsageDataProvider.ID, ProviderPresentationInfo.create("CPU", "", Activator.getImage(Activator.IMAGE_CPU_USAGE)));
	}

	public static class ProviderPresentationInfo {
		public String name;
		public String description;
		public Image image;

		public static ProviderPresentationInfo create(IDataProviderDescriptor descriptor) {
			return create(descriptor.getName(), descriptor.getDescription(), null);
		}

		public static ProviderPresentationInfo create(String name, String description, Image image) {
			ProviderPresentationInfo info = new ProviderPresentationInfo();
			info.name = name;
			info.description = description;
			info.image = image;
			return info;
		}
	}

	public static ProviderPresenter getInstance() {
		return fInstance;
	}

	public ProviderPresentationInfo presentationFor(IDataProviderDescriptor descriptor) {
		return fInfo.computeIfAbsent(descriptor.getId(), id -> ProviderPresentationInfo.create(descriptor));
	}
}
