package org.eclipse.tracecompass.incubator.tracebook.ui.controls;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.tracecompass.incubator.tracebook.ui.providers.ProviderPresenter.ProviderPresentationInfo;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class Section extends Composite {
//	private static final String MOVE_DOWN_IMAGE = "arrow_down.png"; //$NON-NLS-1$
//	private static final String MOVE_UP_IMAGE = "arrow_up.png"; //$NON-NLS-1$

    public static class ToolAction implements SelectionListener {
        @Override
        public void widgetSelected(SelectionEvent e) {
        }

        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }
    }

    SelectionListener moveUp = new ToolAction() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            SashForm sashForm = (SashForm) getParent();
            Control[] children = sashForm.getChildren();
            Control previous = null;
            for (Control control : children) {
                if (control instanceof Sash) {
                    continue;
                }
                if (control.equals(Section.this)) {
                    control.moveAbove(previous);
                    sashForm.layout(true);
                    break;
                }
                previous = control;
            }
        }
    };

    SelectionListener moveDown = new ToolAction() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            SashForm sashForm = (SashForm) getParent();
            Control[] children = sashForm.getChildren();
            Control previous = null;
            for (Control control : children) {
                if (control instanceof Sash) {
                    continue;
                }
                if (control.equals(Section.this)) {
                    previous = control;
                } else if (previous != null) {
                    control.moveAbove(previous);
                    sashForm.layout(true);
                    break;
                }
            }
        }
    };

    SelectionListener remove = new ToolAction() {
        @Override
        public void widgetSelected(SelectionEvent e) {
//        	Composite parent = getParent();
//            dispose();
//            parent.layout(true);
            if (onCloseListener != null) {
            	onCloseListener.widgetDisposed(null);
            }
        }
    };

    static class ConfigureDialog extends Dialog {
    	private DisposeListener onClose;

		protected ConfigureDialog(Shell parentShell, DisposeListener onClose) {
			super(parentShell);
			this.onClose = onClose;
			setShellStyle(SWT.RESIZE | SWT.MODELESS | SWT.CLOSE);
		}
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite composite = (Composite) super.createDialogArea(parent);
			parent.getShell().setText("Configure Section");
			parent.getShell().addDisposeListener(onClose);
			return composite;
		}

		public void onTop() {
			try {
				getContents().getShell().forceActive();
			} catch (Exception e) {
				// ignore exceptions, like disposed widgets
			}
		}
    }
    private ConfigureDialog configureDialog = null;
    private DisposeListener onCloseListener;

	private Runnable onPaintListener;

	SelectionListener configure = new ToolAction() {
        @Override
        public void widgetSelected(SelectionEvent e) {
        	if (configureDialog != null) {
        		configureDialog.onTop();
        	} else {
        		configureDialog = new ConfigureDialog(getShell(),e1 -> configureDialog = null);
        		configureDialog.open();
        	}
        }
    };

	public Section(Composite parent, Color bgColor, ProviderPresentationInfo info) {
		super(parent, SWT.NONE);
		// parent has 3 columns: toolbar, table/tree/text, graph (xy or timechart)
        setLayout(new GridLayout(3, false));
        setBackground(bgColor);
        Composite laneBar = new Composite(this, SWT.NONE);
        laneBar.setBackground(bgColor);
        laneBar.setLayoutData(GridDataFactory.fillDefaults().grab(false, true).create());
        GridLayoutFactory.fillDefaults().applyTo(laneBar);
        ToolBar toolBar = new ToolBar(laneBar, SWT.VERTICAL);
        GridDataFactory.fillDefaults().grab(true, false).applyTo(toolBar);
        if (info.image != null) {
        	createToolItem(toolBar, info.image).addSelectionListener(configure);
        }
//        createToolItem(toolBar, MOVE_UP_IMAGE).addSelectionListener(moveUp);
//        createToolItem(toolBar, MOVE_DOWN_IMAGE).addSelectionListener(moveDown);
        createToolItem(toolBar, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_REMOVE)).addSelectionListener(remove);
        VerticalLabel label = new VerticalLabel(laneBar);
        label.setText(info.name);
        label.setBackground(new Color(240, 240, 240));
        GridDataFactory.fillDefaults().hint(1, 1).grab(true, true).applyTo(label);
	}

    private static ToolItem createToolItem(ToolBar toolBar, Image image) {
        ToolItem toolItem = new ToolItem(toolBar, SWT.PUSH);
        toolItem.setImage(image);
        return toolItem;
    }

	public void onClose(DisposeListener listener) {
		this.onCloseListener = listener;
	}

	public void onPaint(Runnable listener) {
		this.onPaintListener = listener;
	}

	//    private static ToolItem createToolItem(ToolBar toolBar, final String imageFileName) {
//        final Image image = SWTGraphicUtil.createImageFromFile("images/" + imageFileName); //$NON-NLS-1$
//        ToolItem toolItem = createToolItem(toolBar, image);
//        toolItem.setImage(image);
//        SWTGraphicUtil.addDisposer(toolItem, image);
//        return toolItem;
//    }
}
