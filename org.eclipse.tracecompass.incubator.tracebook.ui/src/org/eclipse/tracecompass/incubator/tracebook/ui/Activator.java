package org.eclipse.tracecompass.incubator.tracebook.ui;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class Activator extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "org.eclipse.tracecompass.incubator.tracebook.ui";

	public static final String IMAGE_CPU_USAGE = "icons/cpuusage.png";

    private static @Nullable Activator fPlugin;

    public Activator() {
	}

    // ------------------------------------------------------------------------
    // Accessors
    // ------------------------------------------------------------------------

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static Activator getDefault() {
        Activator plugin = fPlugin;
        if (plugin == null) {
            throw new IllegalStateException();
        }
        return plugin;
    }


    /**
     * Gets the absolute path from a path relative to this plugin's root
     *
     * @param relativePath
     *            The path relative to this plugin
     * @return The absolute path corresponding to this relative path
     */
    public @Nullable IPath getAbsolutePath(Path relativePath) {
        URL location = FileLocator.find(getBundle(), relativePath, null);
        try {
            IPath path = new Path(FileLocator.toFileURL(location).getPath());
            return path;
        } catch (IOException e) {
            return null;
        }
    }

    // ------------------------------------------------------------------------
    // Operators
    // ------------------------------------------------------------------------

    @Override
    public void start(@Nullable BundleContext context) throws Exception {
        super.start(context);
        fPlugin = this;
    }

    @Override
    public void stop(@Nullable BundleContext context) throws Exception {
        fPlugin = null;
        super.stop(context);
    }

    public static Image getImage(String imageId) {
		ImageRegistry imageRegistry = getDefault().getImageRegistry();
		Image image = imageRegistry.get(imageId);
		if (image == null) {
			image = imageDescriptorFromPlugin(PLUGIN_ID, imageId).createImage();
			imageRegistry.put(imageId, image);
		}
		return image;
	}
}
